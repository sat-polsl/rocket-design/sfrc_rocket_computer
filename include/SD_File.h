#ifndef SD_FILE_H
#define SD_FILE_H

#include <SD.h>
#include <SPI.h>

#include <cstdio>

class SD_File {
   public:
    SD_File(char* filename);
    ~SD_File();
    template <typename... T>
    void write(char* formatting, T... args) {
        char buffer[128] = {};
        auto size = std::snprintf(buffer, 128, formatting, args...);
        f.write(buffer, size);
    };

   private:
    File f;
};

#endif