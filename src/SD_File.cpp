#include "../include/SD_File.h"

SD_File::SD_File(char* filename) { f = SD.open(filename, FILE_WRITE); }

SD_File::~SD_File() { f.close(); }